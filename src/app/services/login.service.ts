import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from '@angular/router';

import { Observable }    from 'rxjs/Observable';

import { AuthenticationService } from './authentication.service';
import { ErrorsService } from './errors.service'

import { URL } from '../model/globals'
import { UserData, UserDataResponse } from '../model/user-data'


class LogInData {
    username: string;
    password: string;
};

@Injectable()
export class LogInService {
    constructor(
        private http: Http,
        private router: Router,
        private errorsService: ErrorsService,
        private authenticationService:AuthenticationService
    ) {}
    
    logIn(username:string, password:string): void {
        var loginData: any = {
            username: username,
            password: password
        }

        this.post('/login', loginData, (response) => response as UserDataResponse).subscribe(
        
        response => {
            let userData: UserData = {
                id: "",//TODO
                username: username,
                locations: response.locations || []
            }
            this.authenticationService.saveAuthenticationData(response.token, response.admin, userData);
            this.errorsService.clearErrors();
            this.router.navigate(["/dashboard"]);
        },
        
        error => {
            var msg = error.data || error.text() || error.statusText;
            this.errorsService.setErrors([msg]);
        }
        );
    }

    logout(){
        this.authenticationService.revokeAuthenticationData();
        this.router.navigate(['/']);
    }

    private post<T>(url: string, body: any, converter: (any: any) => T): Observable<T> {
        return this.http.post(URL + url, body)
            .map(response => response.json())
            .map(converter);
    }
}