import { Injectable } from '@angular/core';
import * as io from 'socket.io-client'
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';

@Injectable()
export class SocketService{
    socket:any = null;
    constructor(){
    this.socket = io('http://localhost:3000');
    }

    update(data:any){
        this.socket.emit('update', data);
    }
   
}
 