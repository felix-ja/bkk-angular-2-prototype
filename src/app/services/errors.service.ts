import { Injectable } from '@angular/core';
import { Subject }    from 'rxjs/Subject';

@Injectable()
export class ErrorsService {
  // Observable string sources
  private errors = new Subject<string[]>();

  // Observable string streams
  onErrors$ = this.errors.asObservable();

  // Service message commands
  setErrors(errors: string[]) {
      this.errors.next(errors);
  }

  clearErrors(){
      this.errors.next([]);
  }
}
