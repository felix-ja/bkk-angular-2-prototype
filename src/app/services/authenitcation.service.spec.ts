import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing'

import { UserData } from '../model/user-data'

import { AuthenticationService } from './authentication.service';

describe('AuthenticationService', () => {
  let authenticationService:AuthenticationService = new AuthenticationService();
  let testToken = "TestTokenString";
  let testUser: UserData = {
    id: "",
    username: "UserName",
    locations: ["location1", "location2"]
  }

  class TestResults {
    isAuthenticated: boolean;
    isAdmin: boolean;
    token: string;
    userData: UserData;
  }

  var storageTest:TestResults = null;
  var revokeTest:TestResults  = null;
/*
  beforeEach(() => {
    spyOn(authenticationService, 'saveToken').and.callThrough();
    spyOn(authenticationService, 'revokeToken').and.callThrough();

    spyOn(authenticationService, 'isAuthenticated').and.callThrough();
    spyOn(authenticationService, 'isAdmin').and.callThrough();
    spyOn(authenticationService, 'getToken').and.callThrough();
    spyOn(authenticationService, 'getUserData').and.callThrough();

    //Fill storage
    authenticationService.revokeToken();
    authenticationService.saveToken(testToken, true, testUser);

    storageTest = {
      isAuthenticated: authenticationService.isAuthenticated(),
      isAdmin: authenticationService.isAdmin(),
      token: authenticationService.getToken(),
      userData: authenticationService.getUserData()
    };

    //Clear storage
    authenticationService.revokeToken();    
    revokeTest = {
      isAuthenticated: authenticationService.isAuthenticated(),
      isAdmin: authenticationService.isAdmin(),
      token: authenticationService.getToken(),
      userData: authenticationService.getUserData()
    };

  });


  it ('should create an AuthenticationService object', () => {
    expect(authenticationService != undefined).toBe(true, 'should create AuthenticationService');
  });

  it("tracks that all AuthenticationService methods hav been called", function() {
    expect(authenticationService.saveToken).toHaveBeenCalled();
    expect(authenticationService.revokeToken).toHaveBeenCalledTimes(2);

    expect(authenticationService.isAuthenticated).toHaveBeenCalled();
    expect(authenticationService.isAdmin).toHaveBeenCalled();
    expect(authenticationService.getToken).toHaveBeenCalled();
    expect(authenticationService.getUserData).toHaveBeenCalled();
  });

  it('expecting all values to be stored', () => {
    expect(storageTest.isAuthenticated).toBe(true, 'should be authenticated')
    expect(storageTest.isAdmin).toBe(true, 'should be admin')
    expect(storageTest.token).toEqual(testToken, 'should store token')
    expect(storageTest.userData).toEqual(testUser, 'should store user data')
  });

  it('expecting all values to be deleted', () => {
    expect(revokeTest.isAuthenticated).toBe(false, 'should not be authenticated')
    expect(revokeTest.isAdmin).toBe(false, 'should not be admin')
    expect(revokeTest.token).toBeNull('should delete token')
    expect(revokeTest.userData).toBeNull('should delete user data')
  });
  */
});
