import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { Observable } from 'rxjs/Observable';

import { AuthenticationService } from './authentication.service';
import { ErrorsService } from './errors.service';

import { URL } from '../model/globals'
import { Metadata, MetadataResponse } from '../model/metadata'

@Injectable()
export class MetadataService {
    private metadata: Observable<Metadata>;

    constructor(
        private http: Http,
        private errorsService: ErrorsService,
        private authenticationService:AuthenticationService
        ) {
        authenticationService.authenticationStateChanged$.subscribe(
        isAuthenticated =>{
            this.metadata = this.get(URL + '/metadata', ((json: MetadataResponse) => {
                return new Metadata(json);
            }));
        },
        error => {
            var msg = error.data || error.text() || error.statusText;
            this.errorsService.setErrors([msg]);
        }
        );
    }

     getMetadata(): Observable<Metadata>{
        return this.metadata;
    }

    private get<T>(url: string, converter: (any: any) => T): Observable<T> {
        return this.http.get(url)
            .map(response => response.json())
            .map(converter);
    }
}
