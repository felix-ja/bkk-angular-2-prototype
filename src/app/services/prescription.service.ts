import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers, Response } from '@angular/http';
import { Router } from '@angular/router';

import { Observable } from 'rxjs/Observable';

import { AuthenticationService } from './authentication.service';
import { ErrorsService } from '../services/errors.service';

import { UserData, UsersResponse, NewUser } from '../model/user-data';
import { URL } from '../model/globals';
import { HttpService } from './http.service';
import { SocketService } from './socket.service';
import { BehaviorSubject }    from 'rxjs/BehaviorSubject';

@Injectable()
export class PrescriptionService {
    private prescriptions = new BehaviorSubject<any[]>([]);
    onPrescriptionsUpdate = this.prescriptions.asObservable();
    constructor(
        private http: Http,
        private httpService: HttpService,
        private socketService: SocketService,
        private router: Router,
        private errorsService: ErrorsService,
        private authenticationService:AuthenticationService
    ) {
         httpService.getRequest('/prescriptions', (json:any) =>{
            return json
        }).subscribe( response => {
            this.prescriptions.next(response);
            }, error => {
                this.handleError(error);
            });
        
        //subscribe to socket for updates
        let listener = Observable.fromEvent(this.socketService.socket, 'update');
        listener.subscribe(
            response =>{
                httpService.getRequest('/prescriptions', (json:any) =>{
                return json
                } ).subscribe(
                    response => {
                    this.prescriptions.next(response);
                }, error => {
                this.handleError(error);
             });
        });
    }

    private get<T>(url: string, converter: (any: any) => T): Observable<T> {
        return this.http.get(url)
            .map(response => response.json())
            .map(converter);
    }

    getPrescriptions (): Observable<[UsersResponse]>{
        return this.httpService.getRequest('/prescriptions', (json) =>json );
    }

    getPrescription (id:string): Observable<UsersResponse> {
        return this.httpService.getRequest('/prescriptions/' + id, (r)=>r );
    }

    removePrescription (id:string): Observable<Response> {
        return this.httpService.deleteRequest('/prescriptions/' + id, (response) => response);
    }

    lockAppointment (id: string): Observable<Response>{
        return this.httpService.putRequest('/prescriptions/' + id + '/lock',{}, (response)=>response);
    }

    unlocPrescription (id:string): Observable<Response> {
        return this.httpService.putRequest('/prescriptions/' + id + '/unlock',{}, (response) => response);
    }

    confirmPrescription(id:string) {
        return this.httpService.putRequest('/prescriptions/' + id + '/confirm', null,(r)=>r);
    }

    updateLocation(id:string, loc:string){
        var data ={
            location: loc
        }
        return this.httpService.putRequest('/prescriptions/' + id, data, (r)=>r);
    }

    handleError(error: any) {
        var msg = error.data || error.text() || error.statusText;
        this.errorsService.setErrors([msg]);
    }
}
