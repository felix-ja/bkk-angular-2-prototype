import { Injectable } from '@angular/core';
import { BehaviorSubject }    from 'rxjs/BehaviorSubject';

import { UserData } from '../model/user-data'


const StorageKeys = {
    TOKEN: "token",
    IS_ADMIN: "isAdmin",
    USER_DATA: "userData"
}

@Injectable()
export class AuthenticationService {
    // Observable string sources
    private authenticationState = new BehaviorSubject<boolean>(false);
    private isAdminState = new BehaviorSubject<boolean>(false);
    private userDataState = new BehaviorSubject<UserData>(null);
    private token:string;

    // Observable string streams
    authenticationStateChanged$ = this.authenticationState.asObservable();
    isAdminStateChanged$ = this.isAdminState.asObservable();
    userDataStateChanged$ = this.userDataState.asObservable();

    saveAuthenticationData(token: string, isAdmin: boolean, userData: UserData) {
        localStorage.setItem(StorageKeys.TOKEN, token);
        localStorage.setItem(StorageKeys.IS_ADMIN, String(isAdmin));
        localStorage.setItem(StorageKeys.USER_DATA, JSON.stringify(userData));

        this.token = token;
        this.authenticationState.next(token && token != '');
        this.isAdminState.next(isAdmin);
        this.userDataState.next(userData);
    }

    loadAuthenticationData(){
        let token:string = localStorage.getItem(StorageKeys.TOKEN);
        let isAdmin:boolean = localStorage.getItem(StorageKeys.IS_ADMIN) == 'true';
        let userData:UserData = JSON.parse(localStorage.getItem(StorageKeys.USER_DATA)) as UserData;
        this.token = token;

        let isAuthenticated = token && token != '';

        this.authenticationState.next(isAuthenticated);
        this.isAdminState.next(isAdmin);
        this.userDataState.next(userData);
    }

    revokeAuthenticationData() {
        localStorage.removeItem(StorageKeys.TOKEN);
        localStorage.removeItem(StorageKeys.IS_ADMIN);
        localStorage.removeItem(StorageKeys.USER_DATA);

        this.token = null;
        this.authenticationState.next(false);
        this.isAdminState.next(false);
        this.userDataState.next(null);
    }

    getToken(): string{
      return this.token || localStorage.getItem(StorageKeys.TOKEN);
    }

    getUser(){
        return this.userDataState;
    }
    
    isAdmin(){
        /*return localStorage.getItem(StorageKeys.IS_ADMIN) === 'true';*/
        return localStorage.getItem(StorageKeys.IS_ADMIN) === 'true';
    }
  /*  getLocations(){
        var hasStorage = localStorage.locations && localStorage.locations != 'undefined'
        return hasStorage ? JSON.parse(localStorage.locations) : undefined;
    }*/
}
