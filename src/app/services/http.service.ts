import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import { Router } from '@angular/router';

import { Observable } from 'rxjs/Observable';

import { AuthenticationService } from './authentication.service';
import { ErrorsService } from '../services/errors.service';

import { UserData, UsersResponse, NewUser } from '../model/user-data'
import { URL } from '../model/globals'

@Injectable()
export class HttpService{
  constructor(
      private http: Http,
      private router: Router,
      private errorsService: ErrorsService,
      private authenticationService:AuthenticationService
  ) {}

getHttpHeader(){
  var token = this.authenticationService.getToken();
  var reqArgs = new RequestOptions();
  var headers = new Headers();
  headers.append("Authorization", "Bearer " + token);
  reqArgs.headers = headers;
  return reqArgs;
}

getRequest<T> (url: string, converter: (any: any) => T): Observable<T>{
  return this.http.get(URL + url, this.getHttpHeader())
      .map(response => response.json())
      .map(converter);
}

deleteRequest<T> (url: string, converter: (any: any) => T): Observable<T>{
  return this.http.delete(URL + url, this.getHttpHeader())
      .map(converter);
}

putRequest<T> (url: string, user: any, converter: (any: any) => T): Observable<T>{
  return this.http.put(URL + url, user, this.getHttpHeader())
      .map(converter);
}

postRequest<T> (url: string, user: any, converter: (any: any) => T): Observable<T>{
  return this.http.post(URL + url, user, this.getHttpHeader())
      .map(converter);
}

}
