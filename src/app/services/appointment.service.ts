import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers, Response } from '@angular/http';
import { Router } from '@angular/router';

import { Observable } from 'rxjs/Observable';

import { AuthenticationService } from './authentication.service';
import { ErrorsService } from '../services/errors.service';

import { UserData, UsersResponse, NewUser } from '../model/user-data';
import { URL } from '../model/globals';
import { HttpService } from './http.service';
import { SocketService } from './socket.service';
import { BehaviorSubject }    from 'rxjs/BehaviorSubject';



@Injectable()
export class AppointmentService {

    private appointments = new BehaviorSubject<any[]>([]);
    //[appointments] observable

    onAppointmentsUpdate = this.appointments.asObservable();

    constructor(
        private http: Http,
        private httpService: HttpService,
        private socketService: SocketService,
        private router: Router,
        private errorsService: ErrorsService,
        private authenticationService:AuthenticationService
    ) {
        //initial appointments (http request)
        httpService.getRequest('/appointments', (json:any) =>{
            return json
        } ).subscribe(
            response => {
                this.appointments.next(response);
            }, error => {
                this.handleError(error);
            }
        );
        
        //subscribe to socket for updates
        let listener = Observable.fromEvent(this.socketService.socket, 'update');
        listener.subscribe(
            response =>{
                httpService.getRequest('/appointments', (json:any) =>{
                return json
                } ).subscribe(
                    response => {
                    this.appointments.next(response);
                }, error => {
                    this.handleError(error);
             }
             );
            }
        );

    }

    private get<T>(url: string, converter: (any: any) => T): Observable<T> {
        return this.http.get(url)
            .map(response => response.json())
            .map(converter);
    }

    getAppointments (): Observable<[UsersResponse]>{
        return this.httpService.getRequest('/appointments', (json) =>json );
    }

    getAppointment (id:string): Observable<UsersResponse> {
        return this.httpService.getRequest('/appointments/' + id, (r)=>r );
    }

    removeAppointment (id:string): Observable<Response> {
        return this.httpService.deleteRequest('/appointments/' + id, (response) => response);
    }

    lockAppointment (id: string): Observable<Response>{
        return this.httpService.putRequest('/appointments/' + id + '/lock',{}, (response)=>response);
    }

    unlockAppointment (id:string): Observable<Response> {
        return this.httpService.putRequest('/appointments/' + id + '/unlock',{}, (response) => response);
    }

    confirmAppointment(id:string, date:string, doctor:string) {
        return this.httpService.putRequest('/appointments/' + id + '/confirm/' + date + '/', {doctor: doctor},(r)=>r);
    }
    addDateSuggestions(id:string, dates:string[]) {
        return this.httpService.putRequest('/appointments/' + id + '/suggestions', dates, (r) => r);
    }

    handleError(error: any) {
            var msg = error.data || error.text() || error.statusText;
            this.errorsService.setErrors([msg]);
    }


}
