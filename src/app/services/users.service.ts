import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers, Response } from '@angular/http';
import { Router } from '@angular/router';

import { Observable } from 'rxjs/Observable';

import { AuthenticationService } from './authentication.service';
import { ErrorsService } from '../services/errors.service';

import { UserData, UsersResponse, NewUser } from '../model/user-data';
import { URL } from '../model/globals';
import { HttpService } from './http.service';



@Injectable()
export class UsersService {
    constructor(
        private http: Http,
        private httpService: HttpService,
        private router: Router,
        private errorsService: ErrorsService,
        private authenticationService:AuthenticationService
    ) {}

    getUsers (): Observable<[UsersResponse]>{
      return this.httpService.getRequest('/users', (json) => json as [UsersResponse]);
    }

    getUser (id:string): Observable<UsersResponse> {
        return this.httpService.getRequest('/users/' + id, (r)=>r as UsersResponse );
    }

    saveUser (user:NewUser): Observable<Response> {
        if (user.id) {
            return this.httpService.putRequest('/users/' + user.id, user, (response) => response );
        } else {
            return this.httpService.postRequest('/users/', user, (response) => response );
        }
    }

    removeUser (id:string): Observable<Response> {
        return this.httpService.deleteRequest('/users/' + id, (response) => response);
    }

    handleError(error: any) {
            var msg = error.data || error.text() || error.statusText;
            this.errorsService.setErrors([msg]);
    }
}
