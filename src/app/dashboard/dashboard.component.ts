import { Component } from '@angular/core';
import { AppointmentService } from '../services/appointment.service';
import { PrescriptionService } from '../services/prescription.service';
import { Router } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';
import { NgPluralize } from "./ngpluralize.component";
import { UserData } from '../model/user-data'

@Component({
  selector: 'my-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['../app.component.css']
})

export class DashboardComponent {
result:any = [];
user:UserData;
isAdmin:boolean;
prescriptions:any = [];
appointments:any = [];
appointmentsSubscriptions:any[] = [];
prescriptionsSubscriptions:any[] = [];

constructor(
    private router: Router,
    private prescriptionService: PrescriptionService,
    private appointmentService: AppointmentService,
    private authenticationService: AuthenticationService
  ) {
    this.getAppointmentList();
    this.getPrescriptionList();
  }

  canEdit(row:any) : boolean {
     if (row.user) {
        return row.user === this.authenticationService.getUser || this.authenticationService.isAdmin();     
    } else {
        return true;
     }
  }
  lockAndOpenAppointment(id:any){
    this.appointmentService.lockAppointment(id).subscribe(
      response =>{
        this.router.navigate(['/appointments/'+ id + '/edit']);
      },error =>{
        this.appointmentService.handleError(error);
      }
    );

  }

  lockAndOpenPrescription(id:any){
      this.prescriptionService.lockAppointment(id).subscribe(
      response =>{
        this.router.navigate(['prescriptons/'+id+'/edit']);
      },
      error =>{
        this.prescriptionService.handleError(error);
      }
    );
  }

getAppointmentList(){
  this.appointmentsSubscriptions.push(this.authenticationService.userDataStateChanged$.subscribe(
      response =>{
         this.user = response;
      }
    ));

    this.appointmentsSubscriptions.push(this.authenticationService.isAdminStateChanged$.subscribe(
      response =>{
        this.isAdmin = response;
      }
    ));
    
    this.appointmentsSubscriptions.push(this.appointmentService.onAppointmentsUpdate.subscribe(
    response =>{
    if(this.isAdmin){
      this.appointments = response;
    }else{
      var _appointments:any = [];
      response.map((appointment)=>{
        this.user.locations.map((_location :any )=>{
          if((_location.name) == appointment.location){
            _appointments.push(appointment);
          }
        });
      });
      this.appointments = _appointments;
    }
       
    }
    ));
}

getPrescriptionList(){
  this.prescriptionsSubscriptions.push(this.authenticationService.userDataStateChanged$.subscribe(
      response =>{
         this.user = response;
      }
    ));

    this.prescriptionsSubscriptions.push(this.authenticationService.isAdminStateChanged$.subscribe(
      response =>{
        this.isAdmin = response;
      }
    ));

    this.prescriptionsSubscriptions.push(this.prescriptionService.onPrescriptionsUpdate.subscribe(
    response =>{
    if(this.isAdmin){
      this.prescriptions = response;
    }else{
      var _prescriptions:any = [];
      response.map((prescription)=>{
        if(prescription.location == ""){
              _prescriptions.push(prescription);
        }else{
            this.user.locations.map((_location:any)=>{
            if(_location.name == prescription.location){
            _prescriptions.push(prescription);
          }
        });
        }
      });
      this.prescriptions = _prescriptions;
    }}));
}

}
