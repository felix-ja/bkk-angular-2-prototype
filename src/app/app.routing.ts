import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UsersListComponent } from './users/users.list.component';
import { UserEditComponent } from './users/users.edit.component';
import { AppointmentListComponent } from './appointments/appointmentList.component';
import { AppointmentEditComponent } from './appointments/appointmentEdit.component';
import { PrescriptionListComponent } from './prescriptions/prescriptionList.component';
import { PrescriptionEditComponent } from './prescriptions/prescriptionEdit.component';


const appRoutes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'login', component: LoginComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'users', component: UsersListComponent },
  { path: 'users/edit', component: UserEditComponent },
  { path: 'users/:id', component: UserEditComponent },
  { path: 'appointments', component: AppointmentListComponent },
  { path: 'appointments/:id', component: AppointmentEditComponent },
  { path: 'appointments/:id/edit', component: AppointmentEditComponent },
  { path: 'prescriptions', component: PrescriptionListComponent },
  { path: 'prescriptions/:id', component: PrescriptionEditComponent },
  { path: 'prescriptons/:id/edit', component: PrescriptionEditComponent }
];

export const appRoutingProviders: any[] = [

];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
