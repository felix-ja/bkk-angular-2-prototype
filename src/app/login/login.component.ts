import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { LogInService } from '../services/login.service';

@Component({
  selector: 'bkk-login',
  templateUrl: './login.component.html',
  styleUrls: ['../app.component.css']
})

export class LoginComponent {

  constructor(private loginService: LogInService, private router: Router) {}

  logIn(username: string, password: string): void{
    this.loginService.logIn(username, password);
  }
}
