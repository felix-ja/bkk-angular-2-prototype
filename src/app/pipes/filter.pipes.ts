import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: "filter",
    pure: false
})
export class CustomPipe implements PipeTransform{
    transform(input:any, arg:string){

        if (arg === 'new'){
            return input.filter((item:any) => item.state === 'new' || item.state === 'waiting_for_server');
        }

        if (arg === 'done'){
            return input.filter((item:any) => item.state === 'done');
        }

        if (arg === 'waiting'){
            return (input.filter((item:any) => (item.state === 'waiting_for_client')) );
        }

        if (arg === "state"){
            if(input === 'done'){
               return '\u2713' 
            }
            if (input === 'new') {
                return 'Neu';
            }
            if (input === 'waiting_for_client') {
                return 'Warten auf Patienten';
            }
            if (input === 'waiting_for_server') {
                return 'Warten auf Praxis';
            }
        }
    }
        
}
