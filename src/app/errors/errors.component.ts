import { Component, Input } from '@angular/core';

@Component({
  selector: 'errors-container',
  templateUrl: './errors.component.html'
})

export class ErrorsComponent {

  @Input() errors: any[] = [];

}
