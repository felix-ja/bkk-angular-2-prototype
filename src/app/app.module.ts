import { NgModule } from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule }    from '@angular/http';
import { routing } from './app.routing';

//Services
import { LogInService } from './services/login.service';
import { AuthenticationService } from './services/authentication.service';
import { UsersService } from './services/users.service';
import { ErrorsService } from './services/errors.service';
import { MetadataService } from './services/metadata.service';
import { HttpService } from './services/http.service';
import { AppointmentService } from './services/appointment.service';
import { PrescriptionService } from './services/prescription.service';
import { SocketService } from './services/socket.service';

//Components
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ErrorsComponent } from './errors/errors.component'
import { AppointmentListComponent } from './appointments/appointmentList.component';
import { AppointmentEditComponent } from './appointments/appointmentEdit.component'; 
import { UsersListComponent } from './users/users.list.component';
import { PrescriptionListComponent } from './prescriptions/prescriptionList.component';
import { PrescriptionEditComponent } from './prescriptions/prescriptionEdit.component';
import { UserEditComponent } from './users/users.edit.component';
import { NgPluralize } from "./dashboard/ngpluralize.component";


import { Ng2BootstrapModule } from 'ng2-bootstrap/ng2-bootstrap';
import { CustomPipe } from './pipes/filter.pipes';


@NgModule({
  imports: [
    Ng2BootstrapModule,
    BrowserModule,
    FormsModule,
    HttpModule,
    routing
  ],
  declarations: [
    AppComponent,
    DashboardComponent,
    LoginComponent,
    ErrorsComponent,
    AppointmentListComponent,
    AppointmentEditComponent,
    PrescriptionListComponent,
    PrescriptionEditComponent,
    UserEditComponent,
    UsersListComponent,
    NgPluralize,
    CustomPipe
  ],
  providers: [
    LogInService,
    UsersService,
    ErrorsService,
    MetadataService,
    HttpService,
    AuthenticationService,
    AppointmentService,
    PrescriptionService,
    SocketService
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule {
  constructor(private metadata:MetadataService){}
 }
