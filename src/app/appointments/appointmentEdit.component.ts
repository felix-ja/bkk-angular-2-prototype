import { Component } from '@angular/core';
import { UsersService } from '../services/users.service';
import { ErrorsService } from '../services/errors.service';
import { AppointmentService } from '../services/appointment.service'
import { Router, ActivatedRoute } from '@angular/router';
import { MetadataService } from '../services/metadata.service';

import { Ng2BootstrapModule } from 'ng2-bootstrap/ng2-bootstrap';

@Component({
  selector: 'my-appointmentEdit',
  templateUrl: './appointmentEdit.component.html',
  styleUrls: ['../app.component.css']
})

export class AppointmentEditComponent{
//| filter:doctorLocationFilter
date: Date;
showcalander = false;
routerId: string;
entry:any = null;
metadata = {};
state = {
    edit: false,
    newDates: false,
    dateRange: 1,
    dates: new Array(1),
    doctorWarning: false
};

constructor(
    private route: Router,
    private router: ActivatedRoute,
    private appointmentService: AppointmentService,
    private metadataService: MetadataService
  ) {
    this.date = new Date();
    this.routerId = this.router.snapshot.params['id'];
    this.router.snapshot.url.map(segment => {
        if(!this.state.edit){this.state.edit = segment.path === 'edit'}
    });
    if(this.routerId){
      this.getAppointment(this.routerId)
    }
    this.getMetadata();
    
  }

  getMetadata(){
      this.metadataService.getMetadata().subscribe(
          response => {
              this.metadata = response;
          }
      );
  }

getAppointment(id:any){
    this.appointmentService.getAppointment(id).subscribe(
        response => {
        this.entry = response;
        },
            error =>{
              this.appointmentService.handleError(error)
            }
    );
}

showCalander(){
    if(this.showcalander){
        this.showcalander = false;
    }else{
        this.showcalander = true;
    }
    
}

confirm(id:any, date:any, doctor:any){
    if(doctor === undefined || doctor === ""){
        this.state.doctorWarning = true;
        return;
    }else{
        this.appointmentService.confirmAppointment(id, date, doctor).subscribe(
            response =>{
                this.route.navigate(['/appointments']);
            }, error =>{
                this.appointmentService.handleError(error);
            }
        );
    }
    
}
remove(id:any){
    this.appointmentService.removeAppointment(id).subscribe(
        response =>{
            this.route.navigate(['/appointments']);
        }, error =>{
            this.appointmentService.handleError(error);
        }
    );
}

saveAndConfirm(){
    this.appointmentService.addDateSuggestions(this.entry.id, this.state.dates).subscribe((response)=>{
    },error =>{
        this.appointmentService.handleError(error);
    });
     this.route.navigate(['/appointments']);
}

saveDateSuggestions( _date:any){
    this.state.dates[this.state.dateRange - 1] = _date;
    this.showcalander = false;
}

incrementDateRange(){
    this.state.dateRange = this.state.dateRange + 1;
    this.state.dates.length = this.state.dateRange;
}
cancleDates(){
    this.state.newDates = false;
    this.state.dates = new Array(1);
    this.state.dateRange = 1;
}


}
