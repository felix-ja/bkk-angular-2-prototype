import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AppointmentService } from '../services/appointment.service';
import { AuthenticationService } from '../services/authentication.service';
import { UserData } from '../model/user-data'

@Component({
  selector: 'my-appointmentList',
  templateUrl: './appointmentList.component.html',
  styleUrls: ['../app.component.css']
})

export class AppointmentListComponent {
appointments:any = [];
subscriptions:any[] = [];
user:UserData;
isAdmin:boolean;
//| filter:isNew | orderBy:'-createdAt'
constructor(
    private router: Router,
    private appointmentService: AppointmentService,
    private authenticationService: AuthenticationService
  ) {
    this.subscriptions.push(authenticationService.userDataStateChanged$.subscribe(
      response =>{
         this.user = response;
      }
    ));

    this.subscriptions.push(authenticationService.isAdminStateChanged$.subscribe(
      response =>{
        this.isAdmin = response;
      }
    ));

    
    this.subscriptions.push(appointmentService.onAppointmentsUpdate.subscribe(
    response =>{
    if(this.isAdmin){
      this.appointments = response;
    }else{
      var _appointments:any = [];
     
      response.map((appointment)=>{
        this.user.locations.map((_location :any )=>{
          if((_location.name) == appointment.location){
            _appointments.push(appointment);
          }
        });
      });
      this.appointments = _appointments;
    }
       
    }
    ));

}
 ngOnDestroy() {
    this.subscriptions.map(sub => {sub.unsubscribe()});
  }

  
  isNew(app:any){
    return app.state === 'new';
  }
  
  isWaiting(app:any){
     return app.state.indexOf('waiting') !== -1;
  }

  isDone(app:any){
    return app.state === 'done';
  }

 canEdit(row:any) : boolean {
     if (row.user) {
        return row.user === this.user || this.isAdmin;     
    } else {
        return true;
     }
  }

  unlock(id:any){
     this.appointmentService.unlockAppointment(id)
  }

  remove(id:any){
     this.appointmentService.removeAppointment(id).subscribe(
       response =>{
          this.router.navigate(['/appointments']);
       }
     );
  }

  lockAndOpen(id:any){
    this.appointmentService.lockAppointment(id).subscribe(
      response =>{
        this.router.navigate(['/appointments/'+ id + '/edit']);
      },error =>{
        this.appointmentService.handleError(error);
      }
    );
  }
}
