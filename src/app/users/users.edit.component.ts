import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UsersService } from '../services/users.service';
import { ErrorsService } from '../services/errors.service';
import { UserData, UsersResponse, NewUser } from '../model/user-data';
import { UsersListComponent } from './users.list.component';
import { MetadataService } from '../services/metadata.service';

import { IMultiSelectOption } from 'angular-2-dropdown-multiselect/src/multiselect-dropdown';

@Component({
  selector: 'bkk-users-edit',
  templateUrl: './users.edit.component.html',
  styleUrls: ['../app.component.css']
})

export class UserEditComponent{
  userLocations:string[] = [];
  validpassword = true;
  routerId: string;
  password1: string;
  password2: string;
  entry:any = new NewUser();
  locations:string;
  metadata = {};

  constructor(
    private userService: UsersService,
    private metadataService: MetadataService,
    private route: Router,
    private router: ActivatedRoute
  ) {
    var observable = metadataService.getMetadata();
    observable.subscribe(success=>{
      console.log(success);
    }, error=>{
      console.log(error);
    });

    this.routerId = this.router.snapshot.params['id'];
    if(this.routerId){
      this.userService.getUser(this.routerId).subscribe(
            response => {
                this.entry = response;
            },
            error =>{
              this.userService.handleError(error)
            }
        );
    }
    this.getMetadata();
  }
 getMetadata(){
      this.metadataService.getMetadata().subscribe(
          response => {
              this.metadata = response;
          }
      );
  }
save(){
  if(this.password1 == this.password2){
        this.validpassword = true;
        this.entry.password = this.password1;
        this.entry.locations = this.userLocations;
        this.userService.saveUser(this.entry).subscribe(
          response =>{
            this.route.navigate(['/users']);
          },
          error =>{
            this.userService.handleError(error)
          }
      )
  }else{
    this.validpassword = false
  }
  
}

validatePassword():boolean{
  if(this.password1 == this.password2){
    return this.validpassword = true;
    }else{
      return this.validpassword = false;
    }
  }

  remove(id:string){
     this.userService.removeUser(id).subscribe(
       response =>{
         this.route.navigate(['/users']);
       },
       error =>{
         this.userService.handleError(error)
       }
     );
  }
  
  updateChecked(value:string,event:any){
    if(event.target.checked){
      this.userLocations.push(value);
    }
    else if (!event.target.checked){
      let indexx = this.userLocations.indexOf(value);
      this.userLocations.splice(indexx,1);
    }
  }
  
}
