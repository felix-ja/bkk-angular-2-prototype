import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { UsersService } from '../services/users.service';
import { ErrorsService } from '../services/errors.service';
import { UsersResponse } from '../model/user-data';




@Component({
  selector: 'bkk-users-list',
  templateUrl: './users.list.component.html',
  styleUrls: ['../app.component.css']
})

export class UsersListComponent {
users: UsersResponse[] = [];

  constructor(
    private usersService: UsersService,
    private errorsService: ErrorsService,
    private router: Router
  ) {
    this.getUsers();
  }

   remove(id:string){
     this.usersService.removeUser(id).subscribe(
       response =>{
         //get the updated users
         var index:number = NaN;
         this.users.map(user => {
           if(user.id === id)index = this.users.indexOf(user);
         });

         if(index != NaN){
           this.users.splice(index, 1);
         }
       },
       error =>{
         this.usersService.handleError(error)
       }
     );
   }

getUsers(){
  this.usersService.getUsers().subscribe(
          response => {
              this.users = response;
          },
          error =>{
            this.usersService.handleError(error)
          }
      );
}
onSelect(id:number){
  this.router.navigate(['/users', id]);
}

}
