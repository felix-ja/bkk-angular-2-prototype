import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { PrescriptionService } from '../services/prescription.service';
import { AuthenticationService } from '../services/authentication.service';
import { UserData } from '../model/user-data'

@Component({
  selector: 'my-prescriptionList',
  templateUrl: './prescriptionList.component.html',
  styleUrls: ['../app.component.css']
})

export class PrescriptionListComponent {
//| filter:isNew | orderBy:'-createdAt'
//| filter:isDone | orderBy:['-createdAt', '-updatedAt']
prescriptions:any = [];
subscriptions:any[] = [];
user:UserData;
isAdmin:boolean;

constructor(
    private router: Router,
    private prescriptionService: PrescriptionService,
    private authenticationService: AuthenticationService
  ) {
    this.subscriptions.push(authenticationService.userDataStateChanged$.subscribe(
      response =>{
         this.user = response;
      }
    ));

    this.subscriptions.push(authenticationService.isAdminStateChanged$.subscribe(
      response =>{
        this.isAdmin = response;
      }
    ));

    this.subscriptions.push(prescriptionService.onPrescriptionsUpdate.subscribe(
    response =>{
    if(this.isAdmin){
      this.prescriptions = response;
    }else{
      var _prescriptions:any = [];
      response.map((prescription)=>{
        if(prescription.location == ""){
              _prescriptions.push(prescription);
        }else{
            this.user.locations.map((_location:any)=>{
            if(_location.name == prescription.location){
            _prescriptions.push(prescription);
          }
        });
        }
      });
      this.prescriptions = _prescriptions;
    }}));
  }

  ngOnDestroy() {
    this.subscriptions.map(sub => {sub.unsubscribe()});
  }
  
  canEdit(row:any) : boolean {
     if (row.user) {
        return row.user === this.authenticationService.getUser || this.authenticationService.isAdmin();     
    } else {
        return true;
     }
  }
  
  lockAndOpen(id:string){
    this.prescriptionService.lockAppointment(id).subscribe(
      response =>{
        this.router.navigate(['prescriptons/'+id+'/edit']);
      },
      error =>{
        this.prescriptionService.handleError(error);
      }
    );
    
  }

   remove(id:any){
        this.prescriptionService.removePrescription(id).subscribe(
            response =>{
               // console.log("REMOVE:",response);
            },
            error =>{
                this.prescriptionService.handleError(error);
            }
        );
    }
}
