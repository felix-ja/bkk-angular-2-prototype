import { Component } from '@angular/core';
import { ErrorsService } from '../services/errors.service';
import { PrescriptionService } from '../services/prescription.service'
import { Router, ActivatedRoute } from '@angular/router';
import { MetadataService } from '../services/metadata.service';

import { Ng2BootstrapModule } from 'ng2-bootstrap/ng2-bootstrap';

@Component({
    selector: 'my-prescriptionEdit',
    templateUrl: './prescriptionEdit.component.html',
    styleUrls: ['../app.component.css']
})

export class PrescriptionEditComponent{
    routerId: string;
    entry:any = null;
    metadata = {};
    state = {
        edit: false
    }
    constructor(
        private route : Router,
        private router: ActivatedRoute,
        private prescriptionService: PrescriptionService,
        private metadataService: MetadataService
    ) {
        this.routerId = this.router.snapshot.params['id'];
        this.router.snapshot.url.map(segment => {
            if(!this.state.edit){this.state.edit = segment.path === 'edit'}
        });
        
        if(this.routerId){
        this.getPrescription(this.routerId)
        }

        // Testing Metadata.....
        this.getMetadata();
    }

    getMetadata(){
        this.metadataService.getMetadata().subscribe(
            response => {
                this.metadata = response;
            }
        );
    }

    getPrescription(id:any){
        this.prescriptionService.getPrescription(id).subscribe(
            response => {
            this.entry = response;
            },
                error =>{
                this.prescriptionService.handleError(error);
                }
        );
    }

    selectDelivery(id:string, location: any){
        this.entry.location = location.name || "";
        this.prescriptionService.updateLocation(id,location.name || "").subscribe(
            response =>{
            },
            error =>{
                this.prescriptionService.handleError(error);
            }
        );
    }

    confirm(id:any){
        this.prescriptionService.confirmPrescription(id).subscribe(
            response =>{
                this.route.navigate(['/prescriptions']);
            },
            error =>{
                this.prescriptionService.handleError(error);
            }
        );
    }

    remove(id:any){
        this.prescriptionService.removePrescription(id).subscribe(
            response =>{
                this.route.navigate(['/prescriptions']);
            },
            error =>{
                this.prescriptionService.handleError(error);
            }
        );
    }

    unlock(id:any){
        this.prescriptionService.unlocPrescription(id).subscribe(
            response =>{
                console.log("UNLOCK_RESPONSE:", response);
            },
            error =>{
                this.prescriptionService.handleError(error);
            }
        );
    }

    
}