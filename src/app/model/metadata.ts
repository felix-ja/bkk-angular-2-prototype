export enum ExaminationType{
    Consultation, Prevention, Preexamination, Other
}

export interface Doctor{
    name: string;
    title: string;
    locations: Location[];
}

export interface Examination{
    name: string;
    type: ExaminationType;
    locations: Location[];
}

export interface Location{
    id: string;
    name: string;
    street: string;
    surgeonCategory: string;
    city: string;
    lat: number;
    lng: number;
    mail: string;
    medicsCategory: string;
}

export class Metadata{
    locations: Location[] = [];
    doctors: Doctor[] = [];
    examinations: Examination[] = [];

    constructor(metadataResponse: MetadataResponse){
      //Location array
       metadataResponse.locations.map(_location=>{
            this.locations.push({
                id: _location.id,
                name: _location.name,
                street: _location.street,
                surgeonCategory: _location.surgeonCategory,
                city: _location.city,
                lat: _location.lat,
                lng: _location.lng,
                mail: _location.mail,
                medicsCategory: _location.medicsCategory
            });
        });
       
        //take all doctors from metadataResponse
        metadataResponse.doctors.map(_doctor => {
            //Create a array containing location objects
            var locations:Location[] = [];
            _doctor.locations.map(locationId => {
                //find location by id
               locations.push(this.locations[locationId]); 
            });

            //append the new doctor to the doctors array 
            this.doctors.push({
                name: _doctor.name,
                title: _doctor.title,
                locations: locations
            });
        });

        this.addExamination(metadataResponse.consultations, ExaminationType.Consultation);
        this.addExamination(metadataResponse.preexaminations, ExaminationType.Preexamination);
        this.addExamination(metadataResponse.preventions, ExaminationType.Prevention);
        this.addExamination(metadataResponse.others, ExaminationType.Other);   
    }

    addExamination(array:any[], examinationType:ExaminationType){
            array.map(_consulation=>{

                var locations:Location[] = [];
                (_consulation.locations as string[]).map(locationId => {
                    //find location by id
                    locations.push(this.locations[locationId]); 
                });
                
                this.examinations.push({
                    name: _consulation.name,
                    type: examinationType,
                    locations: locations
                });
        });}
}

export interface MetadataResponse{
    doctors: [{
        name: string;
        title: string;
        locations: string[];
    }];

    locations: [{
        id: string;
        name: string;
        street: string;
        surgeonCategory: string;
        city: string;
        lat: number;
        lng: number;
        mail: string;
        medicsCategory: string;
    }];

    consultations: [{
        name: string;
        locations: string[];
    }];

    preventions: [{
        name: string;
        locations: string[];
    }];

    preexaminations: [{
        name: string;
        locations: string[];
    }];

    others: [{
        name: string;
        locations: string[];
    }];
    

}