//backend user for location sorting and username
export class UserData {
  id: string;
  username: string;
  locations: string[];
}

// url/login response json
export class UserDataResponse {
  token: string;
  admin: boolean;
  locations: string[];
}

// url/users response json
export class UsersResponse{
  admin: boolean;
  email: string;
  id: string;
  locations: [string];
  username: string;
}

export class NewUser{
  id: string;
  username: string;
  email: string;
  locations: [string];
  password: string;
}
