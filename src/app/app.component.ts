import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ErrorsService } from './services/errors.service';
import { AuthenticationService } from './services/authentication.service';
import { ErrorsComponent } from './errors/errors.component';
import { UserData } from './model/user-data';



@Component({
  selector: 'app-main',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  subscriptions:any[] = [];
  errors: string[];
  isAuthenticated: boolean = false;
  isAdmin: boolean = false;
  userData: UserData;
  

  constructor(
    private router: Router,
    private errorsService: ErrorsService, 
    private authenticationService: AuthenticationService
  ){    
    errorsService.onErrors$.subscribe(errors => {
      this.errors = errors;
    });

    this.subscriptions.push(authenticationService.authenticationStateChanged$.subscribe(isAuthenticated => {
      this.isAuthenticated = isAuthenticated;
      if(isAuthenticated){
        this.router.navigate(['/dashboard']);
      }
      else{
        this.router.navigate(['/']);
      }
    }));

    var observable = authenticationService.isAdminStateChanged$.subscribe(isAdmin => {
        this.isAdmin = isAdmin;
      })
    this.subscriptions.push(observable);

    this.subscriptions.push(authenticationService.userDataStateChanged$.subscribe(userData => {
      this.userData = userData;
    }));
  }

  ngAfterViewInit(){
    var scope = this;
    setTimeout(function(){scope.authenticationService.loadAuthenticationData()});
  }

  ngOnDestroy() {
    this.subscriptions.map(sub => {sub.unsubscribe()});
  }

  logout(){
    this.authenticationService.revokeAuthenticationData();
  }

  isActive(location: string){
    //console.log("check", location);
  }
}
