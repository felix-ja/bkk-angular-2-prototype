import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing'

import { AppComponent } from './app.component';
import { ErrorsComponent } from './errors/errors.component'

describe('App', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({ 
      imports: [RouterTestingModule.withRoutes( [ {path: '', component: AppComponent} ] )],
      declarations: [AppComponent, ErrorsComponent]
    });
  });
  it ('should create a component', () => {
    let fixture = TestBed.createComponent(AppComponent);
    expect(fixture.componentInstance instanceof AppComponent).toBe(true, 'should create AppComponent');
  });
});
